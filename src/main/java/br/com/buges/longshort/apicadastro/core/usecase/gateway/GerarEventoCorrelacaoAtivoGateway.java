package br.com.buges.longshort.apicadastro.core.usecase.gateway;

import br.com.buges.longshort.apicadastro.core.usecase.dto.CorrelacaoAtivoRequest;

public interface GerarEventoCorrelacaoAtivoGateway {

	void gerarEvento(CorrelacaoAtivoRequest ativo);

}
