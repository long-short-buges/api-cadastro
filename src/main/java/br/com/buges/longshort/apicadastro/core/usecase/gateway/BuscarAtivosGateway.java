package br.com.buges.longshort.apicadastro.core.usecase.gateway;

import br.com.buges.longshort.apicadastro.core.usecase.dto.PaginadoAtivoResponse;

public interface BuscarAtivosGateway {

	PaginadoAtivoResponse buscarAtivos(Integer pagina, Integer qtdPorPagina);

}
