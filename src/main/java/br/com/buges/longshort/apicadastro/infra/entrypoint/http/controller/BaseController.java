package br.com.buges.longshort.apicadastro.infra.entrypoint.http.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.buges.longshort.apicadastro.core.usecase.AtualizarAtivoUseCase;
import br.com.buges.longshort.apicadastro.core.usecase.BuscarAtivosUseCase;
import br.com.buges.longshort.apicadastro.core.usecase.BuscarPorCodigoAtivoUseCase;
import br.com.buges.longshort.apicadastro.core.usecase.CadastrarAtivoUseCase;
import br.com.buges.longshort.apicadastro.core.usecase.DeletarAtivoUseCase;
import br.com.buges.longshort.apicadastro.core.usecase.dto.AtivoRequest;
import br.com.buges.longshort.apicadastro.core.usecase.dto.AtivoResponse;
import br.com.buges.longshort.apicadastro.core.usecase.dto.PaginadoAtivoResponse;
import br.com.buges.longshort.base.dto.response.BaseResponse;
import br.com.buges.longshort.base.dto.response.ListaErroEnum;
import br.com.buges.longshort.base.dto.response.ResponseDataErro;
import br.com.buges.longshort.base.usecase.BaseUseCase;

/**
 * Controller p acesso aos recursos do ativo ex:
 * http://localhost:8080/ape/v1/ativos (endpoint)
 * 
 * @author ggarc
 *
 */
public abstract class BaseController {
	
	protected Logger log = LoggerFactory.getLogger(this.getClass());

	protected <T extends BaseResponse> ResponseEntity<T> construirResponse(T response) {

		log.debug("Iniciando a construção do response HTTP");

		if (response.getResponse().getErros().isEmpty()) {
			log.info("Resposta sem ERROS (200 - OK)", response);
			return ResponseEntity.ok(response);
		} else {
			log.error("Ocorreram erros ao processar sua requisição", response);
			List<ResponseDataErro> erros = response.getResponse().getErros();
			ResponseDataErro erro = erros.get(0);

			if (erro.getTipo().equals(ListaErroEnum.CAMPOS_OBRIGATORIOS)) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
			} else if (erro.getTipo().equals(ListaErroEnum.DUPLICIDADE)) {
				return ResponseEntity.status(HttpStatus.CONFLICT).body(response);
			} else if (erro.getTipo().equals(ListaErroEnum.ENTIDADE_NAO_ENCONTRADA)) {
				return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
			} else {
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
			}
		}

	}
	
}
