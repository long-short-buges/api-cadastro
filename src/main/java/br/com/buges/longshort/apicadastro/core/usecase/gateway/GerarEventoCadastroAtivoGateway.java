package br.com.buges.longshort.apicadastro.core.usecase.gateway;

import br.com.buges.longshort.apicadastro.core.usecase.dto.AtivoRequest;

public interface GerarEventoCadastroAtivoGateway {

	void gerarEvento(AtivoRequest ativo);
}
