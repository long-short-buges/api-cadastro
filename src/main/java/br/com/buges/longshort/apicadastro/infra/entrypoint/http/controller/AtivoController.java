package br.com.buges.longshort.apicadastro.infra.entrypoint.http.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.buges.longshort.apicadastro.core.usecase.AtualizarAtivoUseCase;
import br.com.buges.longshort.apicadastro.core.usecase.BuscarAtivosUseCase;
import br.com.buges.longshort.apicadastro.core.usecase.BuscarPorCodigoAtivoUseCase;
import br.com.buges.longshort.apicadastro.core.usecase.CadastrarAtivoUseCase;
import br.com.buges.longshort.apicadastro.core.usecase.DeletarAtivoUseCase;
import br.com.buges.longshort.apicadastro.core.usecase.dto.AtivoRequest;
import br.com.buges.longshort.apicadastro.core.usecase.dto.AtivoResponse;
import br.com.buges.longshort.apicadastro.core.usecase.dto.PaginadoAtivoResponse;
import br.com.buges.longshort.base.dto.response.BaseResponse;
import br.com.buges.longshort.base.dto.response.ListaErroEnum;
import br.com.buges.longshort.base.dto.response.ResponseDataErro;
import br.com.buges.longshort.base.usecase.BaseUseCase;

/**
 * Controller p acesso aos recursos do ativo ex:
 * http://localhost:8080/ape/v1/ativos (endpoint)
 * 
 * @author ggarc
 *
 */
@RestController
@RequestMapping("/api/v1/ativos")
public class AtivoController extends BaseController {

	private final CadastrarAtivoUseCase cadastrarUseCase;
	private final DeletarAtivoUseCase deletarAtivoUseCase;
	private final BuscarPorCodigoAtivoUseCase buscarPorCodigoAtivoUseCase;
	private final AtualizarAtivoUseCase atualizarUseCase;
	private final BuscarAtivosUseCase buscarAtivosUseCase;

	public AtivoController(CadastrarAtivoUseCase cadastrarUseCase, DeletarAtivoUseCase deletarAtivoUseCase,
			BuscarPorCodigoAtivoUseCase buscarPorCodigoAtivoUseCase, AtualizarAtivoUseCase atualizarUseCase,
			 BuscarAtivosUseCase buscarAtivosUseCase) {
		this.cadastrarUseCase = cadastrarUseCase;
		this.deletarAtivoUseCase = deletarAtivoUseCase;
		this.buscarPorCodigoAtivoUseCase = buscarPorCodigoAtivoUseCase;
		this.atualizarUseCase = atualizarUseCase;
		this.buscarAtivosUseCase = buscarAtivosUseCase;
	}

	/**
	 * @author ronaldo.lanhellas Cadastra um ativo. Exemplo de chamada válida: POST
	 *         http://localhost:8080/api/v1/ativos/ { "codigo": "PETR4",
	 *         "descricao": "PETROBRAS" }
	 */
	@PostMapping(consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<AtivoResponse> cadastrarAtivo(@RequestBody AtivoRequest request) {
		log.info("Cadastrando ativo " + request.getCodigo(), request);
		AtivoResponse response = cadastrarUseCase.executar(request);
		log.info("Resposta do cadastro para o ativo de código " + response.getCodigo() + ". ID gerado =  "
				+ response.getId(), response);
		return construirResponse(response);
	}

	/**
	 * Realiza a busca do ativo pelo código. Exemplo: GET
	 * http://localhost:8080/api/v1/ativos/PETR4
	 * 
	 * @author ronaldo.lanhellas
	 */
	@GetMapping("/{codigo}")
	public ResponseEntity<AtivoResponse> buscarPorCodigo(@PathVariable("codigo") String codigo) {
		log.info("Buscando ativo pelo código " + codigo);
		AtivoResponse response = buscarPorCodigoAtivoUseCase.executar(codigo);
		log.info("Resposta da busca para o ativo de código " + response.getCodigo() + ". ID =  " + response.getId(),
				response);
		return construirResponse(response);
	}

	/**
	 * @author ronaldo.lanhellas Deleta um ativo. Exemplo de chamada válida: DELETE
	 *         http://localhost:8080/api/v1/ativos/{codigo}
	 */
	@DeleteMapping(value = "/{codigo}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<AtivoResponse> deletarAtivo(@PathVariable("codigo") String codigo) {
		log.info("Deletando ativo " + codigo);
		AtivoResponse response = deletarAtivoUseCase.executar(codigo);
		return construirResponse(response);
	}

	/**
	 * @author ronaldo.lanhellas
	 * PUT http://localhost:8080/api/v1/ativos/PETR4 
	 * MINHA NOVA DESCRICAO
	 */
	@PutMapping(value = "/{codigo}", consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<AtivoResponse> atualizarAtivo(@RequestBody String descricao,
			@PathVariable("codigo") String codigo) {
		AtivoRequest request = new AtivoRequest();
		request.setCodigo(codigo);
		request.setDescricao(descricao);
		AtivoResponse response = atualizarUseCase.executar(request);
		return construirResponse(response);
	}

	/**
	 * Realiza a busca dos ativos. 
	 * Exemplo: 
	 * GET http://localhost:8080/api/v1/ativos?pagina=10
	 * @author ronaldo.lanhellas
	 */
	@GetMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<PaginadoAtivoResponse> buscarAtivos(@RequestParam("pagina") Integer pagina) {
		PaginadoAtivoResponse paginadoResponse = buscarAtivosUseCase.executar(pagina);
		return construirResponse(paginadoResponse);
	}
}
