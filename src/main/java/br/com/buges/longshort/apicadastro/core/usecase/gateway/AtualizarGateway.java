package br.com.buges.longshort.apicadastro.core.usecase.gateway;

public interface AtualizarGateway<ID> {

	Boolean atualizar(ID id);
}
