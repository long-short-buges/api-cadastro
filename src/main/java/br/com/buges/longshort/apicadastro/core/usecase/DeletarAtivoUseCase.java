package br.com.buges.longshort.apicadastro.core.usecase;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import br.com.buges.longshort.apicadastro.core.entity.Ativo;
import br.com.buges.longshort.apicadastro.core.usecase.dto.AtivoRequest;
import br.com.buges.longshort.apicadastro.core.usecase.dto.AtivoResponse;
import br.com.buges.longshort.apicadastro.core.usecase.gateway.BuscarPorCodigoAtivoGateway;
import br.com.buges.longshort.base.dto.response.ListaErroEnum;
import br.com.buges.longshort.base.dto.response.ResponseDataErro;
import br.com.buges.longshort.base.gateway.SalvarGateway;
import br.com.buges.longshort.base.usecase.BaseUseCase;

/**
 * deletar ativo
 * @author ggarc
 *
 */
public interface DeletarAtivoUseCase extends BaseUseCase<String, AtivoResponse> {
}
