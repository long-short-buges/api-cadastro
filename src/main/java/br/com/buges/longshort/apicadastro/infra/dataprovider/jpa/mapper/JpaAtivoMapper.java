package br.com.buges.longshort.apicadastro.infra.dataprovider.jpa.mapper;

import java.util.Objects;

import br.com.buges.longshort.apicadastro.core.entity.Ativo;
import br.com.buges.longshort.apicadastro.infra.dataprovider.jpa.entity.JpaAtivoEntity;

public interface JpaAtivoMapper {

	static JpaAtivoEntity toJpa(Ativo ativo) {
		if (Objects.nonNull(ativo)) {
			JpaAtivoEntity entity = new JpaAtivoEntity();
			entity.setCodigo(ativo.getCodigo());
			entity.setDataHoraCriacao(ativo.getDataHoraCriacao());
			entity.setDescricao(ativo.getDescricao());
			entity.setId(ativo.getId());
			return entity;
		} else {
			return null;
		}

	}
	
	static Ativo toModel(JpaAtivoEntity jpaEntity) {
		if (Objects.nonNull(jpaEntity)) {
			Ativo ativo = new Ativo(jpaEntity.getId());
			ativo.setCodigo(jpaEntity.getCodigo());
			ativo.setDescricao(jpaEntity.getDescricao());
			return ativo;
		} else {
			return null;
		}
	}

}
