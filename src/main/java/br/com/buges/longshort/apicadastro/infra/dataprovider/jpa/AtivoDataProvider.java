package br.com.buges.longshort.apicadastro.infra.dataprovider.jpa;

import java.util.List;
import java.util.Objects;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Repository;

import br.com.buges.longshort.apicadastro.core.entity.Ativo;
import br.com.buges.longshort.apicadastro.core.usecase.dto.PaginadoAtivoResponse;
import br.com.buges.longshort.apicadastro.core.usecase.dto.PaginadoAtivoResponseData;
import br.com.buges.longshort.apicadastro.core.usecase.gateway.BuscarAtivosGateway;
import br.com.buges.longshort.apicadastro.core.usecase.gateway.BuscarPorCodigoAtivoGateway;
import br.com.buges.longshort.apicadastro.infra.configuration.mapper.ObjectMapperUtil;
import br.com.buges.longshort.apicadastro.infra.dataprovider.jpa.entity.JpaAtivoEntity;
import br.com.buges.longshort.apicadastro.infra.dataprovider.jpa.repository.JpaAtivoRepository;
import br.com.buges.longshort.base.gateway.BuscarPorIdGateway;
import br.com.buges.longshort.base.gateway.DeletarGateway;
import br.com.buges.longshort.base.gateway.SalvarGateway;

@Repository
public class AtivoDataProvider
		implements BuscarPorCodigoAtivoGateway, SalvarGateway<Ativo>, 
				   BuscarPorIdGateway<Ativo, String>, DeletarGateway<String>, BuscarAtivosGateway {

	private final JpaAtivoRepository repository;

	public AtivoDataProvider(JpaAtivoRepository repository) {
		this.repository = repository;
	}

	@Override
	public Optional<Ativo> buscarPorId(String id) {
		JpaAtivoEntity jpaAtivoEntity = repository.findById(id).orElse(null);

		if (Objects.nonNull(jpaAtivoEntity)) {
			return Optional.of(ObjectMapperUtil.convertTo(jpaAtivoEntity, Ativo.class));
		} else {
			return Optional.empty();
		}
	}

	@Override
	public Ativo salvar(Ativo t) {
		JpaAtivoEntity entity = repository.save(ObjectMapperUtil.convertTo(t, JpaAtivoEntity.class));
		return ObjectMapperUtil.convertTo(entity, Ativo.class);
	}

	@Override
	public Optional<Ativo> buscarPorCodigoAtivo(String codigo) {
		List<JpaAtivoEntity> jpaAtivos = repository.findByCodigo(codigo);
		
		if(jpaAtivos.isEmpty()) {
			return Optional.empty();
		} else {
			return Optional.of(ObjectMapperUtil.convertTo(jpaAtivos.get(0), Ativo.class));
		}
	}

	@Override
	public Boolean deletar(String codigo) {
		List<JpaAtivoEntity> jpaAtivos = repository.findByCodigo(codigo);
		
		if(!jpaAtivos.isEmpty()) {
			repository.delete(jpaAtivos.get(0));
			return true;
		}
		
		return false;
	}
	
	@Override
	public PaginadoAtivoResponse buscarAtivos(Integer pagina, Integer qtdPorPagina) {
		Page<JpaAtivoEntity> pageJpaAtivo = repository.findAll(PageRequest.of(pagina, qtdPorPagina));
		PaginadoAtivoResponse response = new PaginadoAtivoResponse();
		response.setPaginaAtual(pagina);
		response.setQtdRegistrosDaPagina(pageJpaAtivo.getNumberOfElements());
		response.setQtdRegistrosTotais(Math.toIntExact(pageJpaAtivo.getTotalElements()));
		pageJpaAtivo.getContent().forEach(c -> response
				.adicionarAtivo(new PaginadoAtivoResponseData(c.getId(), c.getCodigo(), c.getDescricao())));

		return response;
	}


}
