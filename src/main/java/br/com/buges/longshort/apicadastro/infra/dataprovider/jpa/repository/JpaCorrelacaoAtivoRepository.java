package br.com.buges.longshort.apicadastro.infra.dataprovider.jpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.buges.longshort.apicadastro.infra.dataprovider.jpa.entity.JpaAtivoEntity;
import br.com.buges.longshort.apicadastro.infra.dataprovider.jpa.entity.JpaCorrelacaoAtivoEntity;

public interface JpaCorrelacaoAtivoRepository extends JpaRepository<JpaCorrelacaoAtivoEntity, String> {

	JpaCorrelacaoAtivoEntity findByAtivo1AndAtivo2(JpaAtivoEntity ativo1, JpaAtivoEntity ativo2);
	
}
