package br.com.buges.longshort.apicadastro.core.usecase;

import br.com.buges.longshort.apicadastro.core.usecase.dto.AtivoResponse;
import br.com.buges.longshort.base.usecase.BaseUseCase;

/**
 * buscar ativo por codigo
 * @author ggarc
 *
 */
public interface BuscarPorCodigoAtivoUseCase extends BaseUseCase<String, AtivoResponse> {
}
