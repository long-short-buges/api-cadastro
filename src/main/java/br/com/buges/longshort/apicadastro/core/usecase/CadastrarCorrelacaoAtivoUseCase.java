package br.com.buges.longshort.apicadastro.core.usecase;

import br.com.buges.longshort.apicadastro.core.usecase.dto.CorrelacaoAtivoRequest;
import br.com.buges.longshort.apicadastro.core.usecase.dto.CorrelacaoAtivoResponse;
import br.com.buges.longshort.base.usecase.BaseUseCase;

public interface CadastrarCorrelacaoAtivoUseCase 
extends BaseUseCase<CorrelacaoAtivoRequest, CorrelacaoAtivoResponse> {
}
