package br.com.buges.longshort.apicadastro.core.entity;

import br.com.buges.longshort.base.entity.BaseEntity;

/**
 * 
 * @author ggarc
 * Reposanvel por manter os ativos do dominio
 * EX. PETR4
 */
public class Ativo extends BaseEntity {
	
	private String codigo;
	private String descricao;
	
	//TODO: colocar dataHoraCriacao como protected
	
	public Ativo() {
	}
	
	public Ativo(String id) {
		this.id = id;
	}
	
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
}
