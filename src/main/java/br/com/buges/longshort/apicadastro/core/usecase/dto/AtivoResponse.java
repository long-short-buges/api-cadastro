package br.com.buges.longshort.apicadastro.core.usecase.dto;

import br.com.buges.longshort.base.dto.response.BaseResponse;

public class AtivoResponse extends BaseResponse {
	/**Reposta para cadastro de ativo
	 * 
	 * @param id
	 * @param codigo
	 * @param descricao
	 */
	
	public AtivoResponse(String id, String codigo, String descricao) {
		super();
		this.id = id;
		this.codigo = codigo;
		this.descricao = descricao;
	}
	
	public AtivoResponse() {
		super();
	}

	private String id;
	private String codigo;
	private String descricao;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

}
