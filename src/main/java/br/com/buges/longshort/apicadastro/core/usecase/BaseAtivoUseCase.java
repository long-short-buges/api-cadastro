package br.com.buges.longshort.apicadastro.core.usecase;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.buges.longshort.apicadastro.core.usecase.dto.AtivoResponse;
import br.com.buges.longshort.apicadastro.core.usecase.dto.AtivoRequest;
import br.com.buges.longshort.base.dto.response.ListaErroEnum;
import br.com.buges.longshort.base.dto.response.ResponseDataErro;

public abstract class BaseAtivoUseCase {
	
	protected Logger logger = LoggerFactory.getLogger(this.getClass());

	protected void validarCamposObrigatorios(AtivoRequest input, AtivoResponse response) {
		if (StringUtils.isBlank(input.getCodigo())) {
			String msg = "O campo código é obrigatorio";
			logger.error(msg);
			response.getResponse().adicionarErro(
					new ResponseDataErro("O campo código é obrigatório", ListaErroEnum.CAMPOS_OBRIGATORIOS));
		}

		if (StringUtils.isBlank(input.getDescricao())) {
			String msg = "O campo descrição é obrigatorio";
			logger.error(msg);
			response.getResponse().adicionarErro(
					new ResponseDataErro("O campo descrição é obrigatório", ListaErroEnum.CAMPOS_OBRIGATORIOS));
		}
	}
}
