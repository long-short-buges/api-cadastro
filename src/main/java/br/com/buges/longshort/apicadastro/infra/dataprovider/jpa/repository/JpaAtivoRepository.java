package br.com.buges.longshort.apicadastro.infra.dataprovider.jpa.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.buges.longshort.apicadastro.infra.dataprovider.jpa.entity.JpaAtivoEntity;

public interface JpaAtivoRepository extends JpaRepository<JpaAtivoEntity, String> {

	//JPQL
	//0 = numero do argumento da lista
	//@Query(value = "SELECT ativo FROM JpaAtivoEntity ativo WHERE ativo.codigo = :0")
	List<JpaAtivoEntity> findByCodigo(String codigo);
	
}
