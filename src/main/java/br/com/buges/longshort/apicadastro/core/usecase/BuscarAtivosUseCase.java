package br.com.buges.longshort.apicadastro.core.usecase;

import br.com.buges.longshort.apicadastro.core.usecase.dto.PaginadoAtivoResponse;
import br.com.buges.longshort.base.usecase.BaseUseCase;

public interface BuscarAtivosUseCase extends BaseUseCase<Integer, PaginadoAtivoResponse> {
}
