package br.com.buges.longshort.apicadastro.core.usecase;

import java.util.Optional;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import br.com.buges.longshort.apicadastro.core.entity.Ativo;
import br.com.buges.longshort.apicadastro.core.usecase.dto.AtivoRequest;
import br.com.buges.longshort.apicadastro.core.usecase.dto.AtivoResponse;
import br.com.buges.longshort.apicadastro.core.usecase.gateway.BuscarPorCodigoAtivoGateway;
import br.com.buges.longshort.base.dto.response.ListaErroEnum;
import br.com.buges.longshort.base.dto.response.ResponseDataErro;
import br.com.buges.longshort.base.gateway.DeletarGateway;
import br.com.buges.longshort.base.gateway.SalvarGateway;

@Service
public class BuscarPorCodigoAtivoUseCaseImpl implements BuscarPorCodigoAtivoUseCase {

	private final BuscarPorCodigoAtivoGateway buscarPorCodigoAtivoGateway;
	private Logger log = LoggerFactory.getLogger(BuscarPorCodigoAtivoUseCaseImpl.class);
	
	public BuscarPorCodigoAtivoUseCaseImpl(BuscarPorCodigoAtivoGateway buscarPorCodigoAtivoGateway) {
		this.buscarPorCodigoAtivoGateway = buscarPorCodigoAtivoGateway;
	}

	@Override
	public AtivoResponse executar(String codigo) {
		log.info("Buscando ativo por codigo "+ codigo);
		return null;
	}

}
