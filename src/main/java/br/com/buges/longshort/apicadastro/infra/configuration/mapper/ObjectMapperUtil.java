package br.com.buges.longshort.apicadastro.infra.configuration.mapper;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

public interface ObjectMapperUtil {

	static ObjectMapper mapper = new ObjectMapper();

	/**
	 * Converte de uma origem p um destino indicado via generics T
	 * @param <T>
	 * @param value
	 * @param clazz
	 * @return
	 */
	static <T> T convertTo(Object value, Class<T> clazz) {
		mapper.registerModule(new JavaTimeModule());
		return mapper.convertValue(value, clazz);
	}
}
