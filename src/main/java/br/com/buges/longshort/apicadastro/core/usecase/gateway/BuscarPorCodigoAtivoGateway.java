package br.com.buges.longshort.apicadastro.core.usecase.gateway;

import java.util.Optional;

import br.com.buges.longshort.apicadastro.core.entity.Ativo;

public interface BuscarPorCodigoAtivoGateway {

	Optional<Ativo> buscarPorCodigoAtivo(String codigo);
	
}
