package br.com.buges.longshort.apicadastro.core.usecase;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import br.com.buges.longshort.apicadastro.core.entity.Ativo;
import br.com.buges.longshort.apicadastro.core.usecase.dto.PaginadoAtivoResponse;
import br.com.buges.longshort.apicadastro.core.usecase.dto.PaginadoAtivoResponseData;
import br.com.buges.longshort.apicadastro.core.usecase.gateway.BuscarAtivosGateway;
import br.com.buges.longshort.apicadastro.core.usecase.gateway.ConfiguracaoGateway;
import br.com.buges.longshort.apicadastro.template.AtivoTemplateLoader;
import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;

@RunWith(MockitoJUnitRunner.class)
public class BuscarAtivoUseCaseImplTest {

	@InjectMocks
	private BuscarAtivosUseCaseImpl usecase;

	@Mock
	private BuscarAtivosGateway buscarAtivosGateway;

	@Mock
	private ConfiguracaoGateway configuracaoGateway;

	@BeforeClass
	public static void beforeClass() {
		FixtureFactoryLoader.loadTemplates("br.com.ronalan.longshort.apicadastro.template");
	}

	@Test
	public void deve_buscar_ativos_paginado() {
		final int quantidadeRegistrosPorPagina = 10;
		final int paginaAtual = 0;
		Mockito.when(configuracaoGateway.getQuantidadeRegistrosPorPagina()).thenReturn(quantidadeRegistrosPorPagina);

		PaginadoAtivoResponse responseMockado = new PaginadoAtivoResponse();

		for (int i = 0; i < quantidadeRegistrosPorPagina; i++) {
			Ativo ativo = Fixture.from(Ativo.class).gimme(AtivoTemplateLoader.RANDOM);
			responseMockado.adicionarAtivo(
					new PaginadoAtivoResponseData(ativo.getId(), ativo.getCodigo(), ativo.getDescricao()));
		}

		responseMockado.setPaginaAtual(paginaAtual);
		responseMockado.setQtdRegistrosDaPagina(quantidadeRegistrosPorPagina);
		responseMockado.setQtdRegistrosTotais(quantidadeRegistrosPorPagina);

		Mockito.when(buscarAtivosGateway.buscarAtivos(paginaAtual, quantidadeRegistrosPorPagina)).thenReturn(responseMockado);

		PaginadoAtivoResponse response = usecase.executar(paginaAtual);

		Assert.assertNotNull(response);
		Assert.assertEquals(Integer.valueOf(paginaAtual), response.getPaginaAtual());
		Assert.assertEquals(Integer.valueOf(quantidadeRegistrosPorPagina), response.getQtdRegistrosDaPagina());
		Assert.assertEquals(Integer.valueOf(quantidadeRegistrosPorPagina), response.getQtdRegistrosTotais());
		Assert.assertEquals(quantidadeRegistrosPorPagina, response.getAtivos().size());
		Assert.assertEquals(0, response.getResponse().getErros().size());
	}
}
