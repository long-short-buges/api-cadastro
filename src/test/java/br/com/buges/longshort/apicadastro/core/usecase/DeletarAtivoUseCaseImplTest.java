package br.com.buges.longshort.apicadastro.core.usecase;

import static org.junit.Assert.assertArrayEquals;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import br.com.buges.longshort.apicadastro.core.entity.Ativo;
import br.com.buges.longshort.apicadastro.core.usecase.dto.AtivoRequest;
import br.com.buges.longshort.apicadastro.core.usecase.dto.AtivoResponse;
import br.com.buges.longshort.apicadastro.core.usecase.gateway.BuscarPorCodigoAtivoGateway;
import br.com.buges.longshort.apicadastro.template.AtivoTemplateLoader;
import br.com.buges.longshort.base.dto.response.ListaErroEnum;
import br.com.buges.longshort.base.gateway.DeletarGateway;
import br.com.buges.longshort.base.gateway.SalvarGateway;
import br.com.buges.longshort.apicadastro.template.AtivoRequestTemplate;
import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;

@RunWith(MockitoJUnitRunner.class)
public class DeletarAtivoUseCaseImplTest {

	@InjectMocks
	private DeletarAtivoUseCaseImpl usecase;

	@Mock
	private DeletarGateway<String> gateway;

	@Mock
	private BuscarPorCodigoAtivoGateway buscarPorCodigoAtivoGateway;

	@Before
	public void before() {
		Mockito.when(buscarPorCodigoAtivoGateway.buscarPorCodigoAtivo(Mockito.anyString())).thenReturn(Optional.of(new Ativo()));
		Mockito.when(gateway.deletar(Mockito.anyString())).thenReturn(true);
	}

	@BeforeClass
	public static void beforeClass() {
		FixtureFactoryLoader.loadTemplates("br.com.buges.longshort.apicadastro.template");
	}

	@Test
	public void deve_deletar_ativo() {
		Ativo ativoPetr4 = Fixture.from(Ativo.class).gimme(AtivoTemplateLoader.PETR4);
		Mockito.when(buscarPorCodigoAtivoGateway.buscarPorCodigoAtivo(ativoPetr4.getCodigo())).thenReturn(Optional.of(ativoPetr4));
		Mockito.when(gateway.deletar(ativoPetr4.getCodigo())).thenReturn(true);

		AtivoResponse response = usecase.executar(ativoPetr4.getCodigo());
	
		Assert.assertEquals(response.getId(), ativoPetr4.getId());
		Assert.assertEquals(response.getCodigo(), ativoPetr4.getCodigo());
		Assert.assertEquals(response.getDescricao(), ativoPetr4.getDescricao());
	}
	
	@Test
	public void nao_deve_deletar_ativo_inexistente() {
		
		Ativo ativoPetr4 = Fixture.from(Ativo.class).gimme(AtivoTemplateLoader.PETR4);
		Mockito.when(buscarPorCodigoAtivoGateway.buscarPorCodigoAtivo(ativoPetr4.getCodigo())).thenReturn(Optional.empty());

		AtivoResponse response = usecase.executar(ativoPetr4.getCodigo());
	
		Assert.assertNull(response.getId());
		Assert.assertEquals(response.getCodigo(), ativoPetr4.getCodigo());
		Assert.assertEquals(1, response.getResponse().getErros().size());
		Assert.assertEquals(ListaErroEnum.ENTIDADE_NAO_ENCONTRADA, response.getResponse().getErros().get(0).getTipo());
	}
	
	@Test
	public void nao_foi_possivel_deletar() {
		
		Ativo ativoPetr4 = Fixture.from(Ativo.class).gimme(AtivoTemplateLoader.PETR4);
		Mockito.when(buscarPorCodigoAtivoGateway.buscarPorCodigoAtivo(ativoPetr4.getCodigo())).thenReturn(Optional.of(ativoPetr4));
		Mockito.when(gateway.deletar(ativoPetr4.getCodigo())).thenReturn(false);

		AtivoResponse response = usecase.executar(ativoPetr4.getCodigo());
		
		Assert.assertNull(response.getId());
		Assert.assertEquals(response.getCodigo(), ativoPetr4.getCodigo());
		Assert.assertNull(response.getDescricao());
		Assert.assertEquals(ListaErroEnum.NAO_FOI_POSSIVEL_DELETAR, response.getResponse().getErros().get(0).getTipo());
	}
}
