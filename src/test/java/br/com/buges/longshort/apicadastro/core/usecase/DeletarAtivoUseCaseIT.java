package br.com.buges.longshort.apicadastro.core.usecase;

import java.util.ArrayList;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.buges.longshort.apicadastro.core.entity.Ativo;
import br.com.buges.longshort.apicadastro.core.usecase.dto.AtivoRequest;
import br.com.buges.longshort.apicadastro.core.usecase.dto.AtivoResponse;
import br.com.buges.longshort.apicadastro.infra.dataprovider.jpa.entity.JpaAtivoEntity;
import br.com.buges.longshort.apicadastro.infra.dataprovider.jpa.repository.JpaAtivoRepository;
import br.com.buges.longshort.apicadastro.template.AtivoTemplateLoader;
import br.com.buges.longshort.base.dto.response.ListaErroEnum;
import br.com.buges.longshort.base.gateway.DeletarGateway;
import br.com.buges.longshort.apicadastro.template.AtivoRequestTemplate;
import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class DeletarAtivoUseCaseIT {

	@BeforeClass
	public static void beforeClass() {
		FixtureFactoryLoader.loadTemplates("br.com.buges.longshort.apicadastro.template");
	}

	@Autowired
	private DeletarAtivoUseCase deletarUseCase;
	
	@Autowired
	private CadastrarAtivoUseCase cadastrarUseCase;
	
	@Autowired
	private JpaAtivoRepository repository;
	
	@SpyBean
	private DeletarGateway<String> deletarGateway;

	@Before
	public void before() {
		repository.deleteAll();
	}
	
	@Test
	public void deve_deletar_ativo() {
		AtivoRequest ativoPetr4 = Fixture.from(AtivoRequest.class).gimme(AtivoTemplateLoader.PETR4);
		AtivoResponse response = cadastrarUseCase.executar(ativoPetr4);
		Assert.assertEquals(1, repository.count());
		
		AtivoResponse responseDeletar = deletarUseCase.executar(response.getCodigo());
		Assert.assertEquals(0, repository.count());
		
		Assert.assertEquals(response.getId(), responseDeletar.getId());
		Assert.assertEquals(response.getCodigo(), responseDeletar.getCodigo());
		Assert.assertEquals(response.getDescricao(), responseDeletar.getDescricao());
	}
	
	@Test
	public void nao_deve_deletar_ativo_inexistente() {
		
		AtivoRequest ativoPetr4 = Fixture.from(AtivoRequest.class).gimme(AtivoTemplateLoader.PETR4);
		
		AtivoResponse responseDeletar = deletarUseCase.executar(ativoPetr4.getCodigo());
	
		Assert.assertNull(responseDeletar.getId());
		Assert.assertEquals(responseDeletar.getCodigo(), ativoPetr4.getCodigo());
		Assert.assertEquals(1, responseDeletar.getResponse().getErros().size());
		Assert.assertEquals(ListaErroEnum.ENTIDADE_NAO_ENCONTRADA, responseDeletar.getResponse().getErros().get(0).getTipo());
	}
	
	@Test
	public void nao_foi_possivel_deletar() {
		
		Mockito.when(deletarGateway.deletar(Mockito.anyString())).thenReturn(false);
	
		AtivoRequest ativoPetr4 = Fixture.from(AtivoRequest.class).gimme(AtivoTemplateLoader.PETR4);
		AtivoResponse response = cadastrarUseCase.executar(ativoPetr4);
		Assert.assertEquals(1, repository.count());
		
		AtivoResponse responseDeletar = deletarUseCase.executar(response.getCodigo());
		Assert.assertEquals(1, repository.count());
			
		Assert.assertNull(responseDeletar.getId());
		Assert.assertEquals(responseDeletar.getCodigo(), ativoPetr4.getCodigo());
		Assert.assertEquals(1, responseDeletar.getResponse().getErros().size());
		Assert.assertEquals(ListaErroEnum.NAO_FOI_POSSIVEL_DELETAR, responseDeletar.getResponse().getErros().get(0).getTipo());
	}
}
