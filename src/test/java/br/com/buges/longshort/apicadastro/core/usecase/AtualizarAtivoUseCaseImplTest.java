package br.com.buges.longshort.apicadastro.core.usecase;

import static org.junit.Assert.assertArrayEquals;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import br.com.buges.longshort.apicadastro.core.entity.Ativo;
import br.com.buges.longshort.apicadastro.core.usecase.dto.AtivoRequest;
import br.com.buges.longshort.apicadastro.core.usecase.dto.AtivoResponse;
import br.com.buges.longshort.apicadastro.core.usecase.gateway.BuscarPorCodigoAtivoGateway;
import br.com.buges.longshort.apicadastro.template.AtivoRequestTemplate;
import br.com.buges.longshort.apicadastro.template.AtivoTemplateLoader;
import br.com.buges.longshort.base.dto.response.ListaErroEnum;
import br.com.buges.longshort.base.gateway.SalvarGateway;
import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;

@RunWith(MockitoJUnitRunner.class)
public class AtualizarAtivoUseCaseImplTest {

	@InjectMocks
	private AtualizarAtivoUseCaseImpl atualizarUseCase;

	@Mock
	private SalvarGateway<Ativo> gateway;

	@Mock
	private BuscarPorCodigoAtivoGateway buscarPorCodigoAtivoGateway;

	@Before
	public void before() {
		Mockito.when(gateway.salvar(Mockito.any())).thenAnswer(i -> i.getArgument(0));
	}

	@BeforeClass
	public static void beforeClass() {
		FixtureFactoryLoader.loadTemplates("br.com.buges.longshort.apicadastro.template");
	}

	@Test
	public void deve_atualizar_ativo() {
		Ativo ativo = Fixture.from(Ativo.class).gimme(AtivoTemplateLoader.PETR4);
		String descricaoAntiga = ativo.getDescricao();
		Mockito.when(buscarPorCodigoAtivoGateway.buscarPorCodigoAtivo(ativo.getCodigo())).thenReturn(Optional.of(ativo));
		
		AtivoRequest request = new AtivoRequest();
		request.setCodigo(ativo.getCodigo());
		request.setDescricao("Atualizei a descrição");
		
		AtivoResponse response = atualizarUseCase.executar(request);

		Assert.assertNotNull(response);
		Assert.assertNotNull(response.getId());
		Assert.assertEquals(request.getCodigo(), response.getCodigo());
		Assert.assertNotEquals(descricaoAntiga, response.getDescricao());
		Assert.assertEquals("Atualizei a descrição", response.getDescricao());
	}

	@Test
	public void nao_deve_atualizar_ativo_inexistente() {
		Mockito.when(buscarPorCodigoAtivoGateway.buscarPorCodigoAtivo(Mockito.anyString()))
				.thenReturn(Optional.empty());

		AtivoRequest request = new AtivoRequest();
		request.setDescricao("DESCRICAO");
		request.setCodigo("AA");
		
		AtivoResponse response = atualizarUseCase.executar(request);

		Assert.assertNotNull(response);
		Assert.assertNull(response.getId());
		Assert.assertEquals(request.getCodigo(), response.getCodigo());
		Assert.assertEquals(request.getDescricao(), response.getDescricao());
		Assert.assertEquals(1, response.getResponse().getErros().size());
		Assert.assertEquals(ListaErroEnum.ENTIDADE_NAO_ENCONTRADA, response.getResponse().getErros().get(0).getTipo());
	}
	
	@Test
	public void nao_deve_atualizar_ativo_campos_ausentes() {
		AtivoRequest request = new AtivoRequest();
		request.setCodigo("      ");
		request.setDescricao("");
		AtivoResponse response = atualizarUseCase.executar(request);
		
		Assert.assertNotNull(response);
		Assert.assertNull(response.getId());
		
		Assert.assertNotNull(response.getResponse());
		Assert.assertEquals(2, response.getResponse().getErros().size());
		
		Assert.assertEquals(ListaErroEnum.CAMPOS_OBRIGATORIOS, response.getResponse().getErros().get(0).getTipo());
		Assert.assertEquals(ListaErroEnum.CAMPOS_OBRIGATORIOS, response.getResponse().getErros().get(0).getTipo());
	}
}
