package br.com.buges.longshort.apicadastro.template;

import br.com.buges.longshort.apicadastro.core.entity.Ativo;
import br.com.buges.longshort.apicadastro.core.usecase.dto.AtivoRequest;
import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;

public class AtivoTemplateLoader implements TemplateLoader{
	
	public static final String RANDOM = "RANDOM";
	public static final String PETR4 = "PETR4";

	@Override
	public void load() {
		Fixture.of(Ativo.class).addTemplate(RANDOM, new Rule() {
			{
				add("codigo", firstName());
				add("descricao", name());
			}
		});

		Fixture.of(Ativo.class).addTemplate(PETR4, new Rule() {
			{
				add("codigo", "PETR4");
				add("descricao", "PETROBRAS");
			}
		});
	}

}
